﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyExample
{
    public class BookStats
    {
        private string[] words;

        public string term="madame";

        public BookStats(string[] w)
        {
            words = w;
        }
        public void GetCountForWord()
        {
            var findWord = from word in words
                           where word.ToUpper().Contains(term.ToUpper())
                           select word;

            Console.WriteLine(@"Thread 1 -- The word ""{0}"" occurs {1} times.",
                term, findWord.Count());
        }

        public  void GetMostCommonWords()
        {
            var frequencyOrder = from word in words
                                 where word.Length > 6
                                 group word by word into g
                                 orderby g.Count() descending
                                 select g.Key;

            var commonWords = frequencyOrder.Take(10);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Thread 2 -- The most common words are:");
            foreach (var v in commonWords)
            {
                sb.AppendLine("  " + v);
            }
            Console.WriteLine(sb.ToString());
        }

        public  void GetLongestWord()
        {
            var longestWord = (from w in words
                               orderby w.Length descending
                               select w).First();

            Console.WriteLine("Thread 3 -- The longest word is {0}", longestWord);
            
        }


    }


    class Program
    {
        static void Main()
        {
            string[] words = CreateWordArray("book.txt");
            var timer = new Stopwatch();
            timer.Start();
            GetLongestWord(words);
            GetMostCommonWords(words);
            GetCountForWord(words, "madame");
            timer.Stop();
            Console.WriteLine("Single thread time ={0}", timer.ElapsedMilliseconds);
            //---------------------------------------------------------------------
            timer.Reset();
            timer.Start();
            BookStats bs = new BookStats(words);
            Thread tr1 = new Thread(new ThreadStart(bs.GetLongestWord));
            tr1.Start();
            var tr2= new Thread(bs.GetCountForWord);
            tr2.Start();
            var tr3 = new Thread(bs.GetMostCommonWords);
            tr3.Start();
            while (tr1.IsAlive || tr2.IsAlive || tr3.IsAlive)
            {
                continue;
            }
            timer.Stop();
            Console.WriteLine("Thread time ={0}", timer.ElapsedMilliseconds);

            //---------------------------------------------------------------------
            timer.Reset();
            timer.Start();
            Parallel.Invoke(() =>
            {
                GetLongestWord(words);
            },  // close first Action

             () =>
             {
                 GetMostCommonWords(words);
             }, //close second Action

             () =>
             {
                 GetCountForWord(words, "madame");
             } //close third Action
         ); //close parallel.invoke
            timer.Stop();
            Console.WriteLine("Parallel time ={0}", timer.ElapsedMilliseconds);
            //---------------------------------------------------------------------
            timer.Reset();
            timer.Start();
            var frequencyOrder = from word in words.AsParallel() 
                                 where word.Length > 6
                                 group word by word into g
                                 orderby g.Count() descending
                                 select g.Key;

            var commonWords = frequencyOrder.Take(10);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("PLINQ 2 -- The most common words are:");
            foreach (var v in commonWords)
            {
                sb.AppendLine("  " + v);
            }
            Console.WriteLine(sb.ToString());
            var longestWord = (from w in words.AsParallel()
                               orderby w.Length descending
                               select w).First();

            Console.WriteLine("PLINQ 1 -- The longest word is {0}", longestWord);
            var term = "madame";
            var findWord = from word in words.AsParallel()
                           where word.ToUpper().Contains(term.ToUpper())
                           select word;

            Console.WriteLine(@"PLINQ 3 -- The word ""{0}"" occurs {1} times.",
                term, findWord.Count());
            timer.Stop();
            Console.WriteLine("PLINQ time ={0}", timer.ElapsedMilliseconds);

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        #region HelperMethods
        private static void GetCountForWord(string[] words, string term)
        {
            var findWord = from word in words
                           where word.ToUpper().Contains(term.ToUpper())
                           select word;

            Console.WriteLine(@"Task 3 -- The word ""{0}"" occurs {1} times.",
                term, findWord.Count());
        }

        private static void GetMostCommonWords(string[] words)
        {
            var frequencyOrder = from word in words
                                 where word.Length > 6
                                 group word by word into g
                                 orderby g.Count() descending
                                 select g.Key;

            var commonWords = frequencyOrder.Take(10);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Task 2 -- The most common words are:");
            foreach (var v in commonWords)
            {
                sb.AppendLine("  " + v);
            }
            Console.WriteLine(sb.ToString());
        }

        private static string GetLongestWord(string[] words)
        {
            var longestWord = (from w in words
                               orderby w.Length descending
                               select w).First();

            Console.WriteLine("Task 1 -- The longest word is {0}", longestWord);
            return longestWord;
        }


        // An http request performed synchronously for simplicity.
        static string[] CreateWordArray(string fileName)
        {
            string s = System.IO.File.ReadAllText(fileName );

             // Separate string into an array of words, removing some common punctuation.
            return s.Split(
                new char[] { ' ', '\u000A', ',', '.', ';', ':', '-', '_', '/','!','?','\n','\r' },
                StringSplitOptions.RemoveEmptyEntries);
        }
        #endregion
    }


}
